# MONK MMD
MONK MMD is a Python module developed using algorithms presented in the article [MONK Outlier-Robust Mean Embedding Estimation by Median-of-Means][MONK_PMLR]. 

This code is used to compute the Maximum Mean Discrepency (MMD) using the outlier-robust MONK BCD algorithms.

###  Dependencies
1. SCIPY
2. NUMPY

For running the examples Matplotlib, scikit-learn and [ITE][ITE] are required.

### Basic example
Using for example as a kernel function, pairwise_kernels from sklearn library

    from sklearn.metrics.pairwise import pairwise_kernels
    def kernel(x,y=None): 
        return pairwise_kernels(x,y,'polynomial',degree=2)
    mmd=MMD_MOM(Q=5,kernel=kernel)
    print(mmd.estimate(np.random.normal(size=[100,1]),np.random.normal(size=[100,1])))
    
### Example
We provide a notebook (Illustration_notebook.ipynb) with examples from the [article][MONK_PMLR]. 

It is viewable directly from the bitbucket [here][notebook] or it can be run locally.

Before running the notebook make sure that the dependencies are installed (Matplotlib and scikit-learn are available through pip and ITE is available [here][ITE], just download the package and unzip it in the same folder as the notebook). The notebook have been tested only with Python3.

[notebook]: https://bitbucket.org/TimotheeMathieu/monk-mmd/src/master/Illustration_notebook.ipynb?viewer=nbviewer
[MONK_PMLR]: http://proceedings.mlr.press/v97/lerasle19a.html
[ITE]: https://bitbucket.org/szzoli/ite-in-python/
